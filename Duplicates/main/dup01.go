package main

import (
	"bufio"
	"os"
	"fmt"
)

// Goal: 		Search for duplicated liens, display them.
// Solution: 	Stdin as a text source, counting lines, append into map[string]int, displaying duplicates

func main() {
	counter := make(map[string]int)
	input := bufio.NewScanner(os.Stdin)

	for input.Scan() {
		counter[input.Text()]++
		if input.Text() == "" {
			break
		}
	}

	//Warning! Ignoring possible input errors.

	for line, value := range counter {
		if value > 1 {
			fmt.Printf("%d\t%s\n", value, line)
		}

	}
}
