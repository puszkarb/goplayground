package main

import (
	"os"
	"bufio"
	"fmt"
)

//Goals:		Finding duplicated lines in given file or from sti.
//Solution: 	Same as in dup01, however Scanner is used more widely.

func main() {
	counts := make(map[string]int)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)

			if err != nil {
				fmt.Printf("File: %s\n\tProblem: %s\n", arg, err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}

	}

	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}

}

func countLines(f *os.File, counts map[string]int) {
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		counts[scanner.Text()]++
		if scanner.Text() == "" {
			break
		}
	}
}
