package main


/* Hello!,
As it comes to the Go programming, all of my basic programs will be inspired by the book :
The Go Programming Language written by A.Donovan and B. Kernighan.

Each piece of code will also be written sticking to the rules depicted in aforementioned lecture.

Please bear in mind - each section is created for presentational and training purposes only!
(That is why the roots name is GoPlayground)

If any project appears, it will always contain annotation "Project"

Enjoy ;)
 */



import "fmt"

func main(){
	fmt.Println("Let's rule the Go world !!!")
}
