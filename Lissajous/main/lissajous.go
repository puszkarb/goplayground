package main

import (
	"image/color"
	"os"
	"io"
	"math/rand"
	"image/gif"
	"image"
	"math"
)

//Goals: 		Create GIF presenting birth of Lissajous' Figures.
//Solution: 	use your image/gif powers.

var palette = []color.Color{
color.White, color.RGBA{
	R: 95,
	G: 224,
	B: 232,
	A: 0,
}, color.RGBA{
	R: 255,
	G: 110,
	B: 255,
	A: 0,
}, color.RGBA{
	R: 64,
	G: 255,
	B: 0,
	A: 0,
}, color.RGBA{
	R: 255,
	G: 71,
	B: 26,
	A: 0,
},color.RGBA {
	R: 0,
	G: 0,
	B: 255,
	A: 0,
}, color.RGBA{
		R: 0,
		G: 0,
		B: 0,
		A: 0,
	}}

const (
	whiteIndex = 0
	blackIndex = 6
)

func main() {
	lissajous(os.Stdout)
}

func lissajous(out io.Writer) {
	const (
		cycles  = 5     // quantity of periods
		res     = 0.001 // angular resolution
		size    = 100   //painting size [-size ... +size]
		nframes = 64    // frames number
		delay   = 10     // delay between each frame - unit is multiplied by 10
	)
	freq := rand.Float64() * 2.0 //oscillators frequency
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase shift
	for i := 0; i < nframes; i ++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles * 2 * math.Pi; t+= res{
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			index := int(rand.Float64()* 5) + 1
			img.SetColorIndex(size+int(x*size+0.5),size+int(y*size+0.5), uint8(index))
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out,&anim)
}
