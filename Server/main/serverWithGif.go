package main

import (
	"sync"
	"net/http"
	"log"
	"fmt"
	"io"
	"image/gif"
	"image"
	"math"
	"math/rand"
	"image/color"
)


var mu sync.Mutex
var count int
var palette = []color.Color{color.White, color.Black}
const(
	blackIndex = 1
	whiteIndex = 0
)

func main(){

	handler := func(w http.ResponseWriter, r *http.Request){
		lissajous(w)
	}


	http.HandleFunc("/", handler)
	http.HandleFunc("/count",counter)

	log.Fatal(http.ListenAndServe("localhost:8000",nil))

}

func handler(w http.ResponseWriter, r *http.Request){
	mu.Lock()
	count++
	mu.Unlock()
	fmt.Fprintf(w, "%s %s %s\n", r.Method, r.URL, r.Proto)
	for k,v := range r.Header  {
		fmt.Fprintf(w,"Header[%q] = %q\n",k,v)
	}

	fmt.Fprintf(w,"Host = %q\n", r.Host)
	fmt.Fprintf(w,"RemoteAddr = %q\n", r.RemoteAddr)
	if err:= r.ParseForm(); err != nil {
		log.Print(err)
	}

	for k,v := range r.Form{
		fmt.Fprintf(w, "Form[%q] = %q\n", k, v )
	}

}

func counter(w http.ResponseWriter, r *http.Request){
	mu.Lock()
	fmt.Fprintf(w,"Views counter: %d\n", count)
	mu.Unlock()
}



func lissajous(out io.Writer) {
	const (
		cycles  = 5     // quantity of periods
		res     = 0.001 // angular resolution
		size    = 100   //painting size [-size ... +size]
		nframes = 64    // frames number
		delay   = 10     // delay between each frame - unit is multiplied by 10
	)
	freq := rand.Float64() * 4.0 //oscillators frequency
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase shift
	for i := 0; i < nframes; i ++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles * 2 * math.Pi; t+= res{
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)

			img.SetColorIndex(size+int(x*size+0.5),size+int(y*size+0.5), blackIndex)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out,&anim)
}
