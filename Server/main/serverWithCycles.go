package main

import (
	"image/color"
	"net/http"
	"log"
	"io"
	"image/gif"
	"image"
	"math"
	"math/rand"
	"strings"
	"strconv"
)



var palette = []color.Color{color.White, color.Black}
const(
	blackIndex = 1
	whiteIndex = 0
)

func main(){

	handler := func(w http.ResponseWriter, r *http.Request){
		lissajous(w,r)
	}


	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8000",nil))

}

func lissajous(out io.Writer, r *http.Request) {
	const (

		res     = 0.001 // angular resolution
		size    = 100   //painting size [-size ... +size]
		nframes = 64    // frames number
		delay   = 10     // delay between each frame - unit is multiplied by 10
	)

	adress := strings.Split(r.URL.String(),"?cycles=")
	cycles := 5
	if len(adress) > 1 {
		c, err := strconv.Atoi(adress[1])
		if err != nil {
			cycles = 5
		} else {
			cycles = c
		}
	}


	freq := rand.Float64() * 4.0 //oscillators frequency
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase shift
	for i := 0; i < nframes; i ++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < float64(cycles) * 2 * math.Pi; t+= res{
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)

			img.SetColorIndex(size+int(x*size+0.5),size+int(y*size+0.5), blackIndex)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out,&anim)
}

