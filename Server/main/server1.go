package main

import (
	"net/http"
	"fmt"
	"log"
	"sync"
)

var mu sync.Mutex
var count int

func main(){
	http.HandleFunc("/",handler)
	http.HandleFunc("/count",counter)
	log.Fatal(http.ListenAndServe("localhost:8000",nil))
}

//handler return component Path requested URL address
func handler(w http.ResponseWriter, r *http.Request){
	mu.Lock()
	count++
	mu.Unlock()
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
}

//counter returns number of calls made so far
func counter(w http.ResponseWriter, r* http.Request){
	mu.Lock()
	fmt.Fprintf(w, "Liczba wywołań: %d\n",count )
	mu.Unlock()
}