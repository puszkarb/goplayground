package main

import (
	"image/color"
	"io"
	"image/gif"
	"image"
	"os"
)

//Goals: 		Create GIF depicting "Glider" - animation should be
// 				based on primitive Cellular Automatic.
//Solution: 	Basing on previous training dir ~/Lissajous use image/gif
// 				import.

var palette = []color.Color{color.White, color.Black}

const (

	whiteIndex = 0
	blackIndex = 1
)

func main() {

	tab := [5][5]uint8{
		{1, 0, 0, 0, 0},
		{0, 1, 1, 0, 0},
		{1, 1, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0}}

	animate(os.Stdout, tab)

}
func animate(out io.Writer, tab [5][5]uint8) {
	temp := [5][5]uint8{}

	const (
		nframes = 64
		delay   = 8
		size    = 5
	)

	anim := gif.GIF{LoopCount: nframes}

	for i := 0; i < nframes; i++ {
		rect := image.Rect(0,0, size , size )
		img := image.NewPaletted(rect, palette)


		for j := 0; j < 5; j++ {
			for k := 0; k < 5; k++ {
				if count(tab, k, j) == 3 && tab[j][k] == 0 {
					temp[j][k] = 1
					/*fmt.Printf("1 ")*/ img.SetColorIndex(k, j, blackIndex)
				} else {
					if tab[j][k] == 1 && (count(tab, k, j) == 2 || count(tab, k, j) == 3) {
						temp[j][k] = 1
						/*fmt.Printf("1 ")*/ img.SetColorIndex(k, j, blackIndex)
					} else {
						temp[j][k] = 0
						/*fmt.Printf("0 ")*/ img.SetColorIndex(k ,j, whiteIndex)
					}
				}

			}
		}
		/*

		for j := 0 ; j < size ; j++{
			if i%2 == 0 && j % 2 == 0{
				img.SetColorIndex(j,j,blackIndex)
			}
		}
		*/
		tab = temp

		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)

	}

	gif.EncodeAll(out, &anim)
}

func count(tab [5][5]uint8, x int, y int) (int) {
	counter := 0
	if x == 0 {
		//top left corner
		if y == 0 {
			for i := 0; i <= 1; i++ {
				for j := 0; j <= 1; j++ {
					if i == 0 && j == 0 {
						continue
					} else {
						if tab[y+j][x+i] == 1 {
							counter++
						}
					}
				}
			}
		} else {
			//left edge
			if y < 4 {
				for i := 0; i <= 1; i++ {
					for j := -1; j <= 1; j++ {
						if i == 0 && j == 0 {
							continue
						}
						if tab[y+j][x+i] == 1 {
							counter++
						}

					}
				}

			} else {
				//bottom left corner
				for i := 0; i <= 1; i++ {
					for j := -1; j <= 0; j++ {
						if i == 0 && j == 0 {
							continue
						}
						if tab[y+j][x+i] == 1 {
							counter++
						}

					}
				}
			}
		}
	} else {
		//top edge
		if y == 0 && x < 4 {
			for i := -1; i <= 1; i++ {
				for j := 0; j <= 1; j++ {
					if i == 0 && j == 0 {
						continue
					}
					if tab[y+j][x+i] == 1 {
						counter++
					}

				}
			}
		} else {
			//right top corner
			if y == 0 && x == 4 {
				for i := -1; i <= 0; i++ {
					for j := 0; j <= 1; j++ {
						if i == 0 && j == 0 {
							continue
						}
						if tab[y+j][x+i] == 1 {
							counter++
						}

					}
				}
			} else {
				//bottom right corner
				if y == 4 && x == 4 {
					for i := -1; i <= 0; i++ {
						for j := 0; j <= 0; j++ {
							if i == 0 && j == 0 {
								continue
							}
							if tab[y+j][x+i] == 1 {
								counter++
							}

						}
					}
				} else {
					//right edge
					if x == 4 && y < 4 {
						for i := -1; i <= 0; i++ {
							for j := -1; j <= 1; j++ {
								if i == 0 && j == 0 {
									continue
								}
								if tab[y+j][x+i] == 1 {
									counter++
								}

							}
						}
					} else {
						//bottom edge
						if y == 4 {
							for i := -1; i <= 1; i++ {
								for j := -1; j <= 0; j++ {
									if i == 0 && j == 0 {
										continue
									}
									if tab[y+j][x+i] == 1 {
										counter++
									}

								}
							}
						} else {
							// middle
							for i := -1; i <= 1; i++ {
								for j := -1; j <= 1; j++ {
									if i == 0 && j == 0 {
										continue
									}
									if tab[y+j][x+i] == 1 {
										counter++
									}

								}
							}
						}
					}
				}
			}
		}

	}

	return counter

}
