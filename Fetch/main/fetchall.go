package main

import (
	"time"
	"os"
	"fmt"
	"net/http"
	"io"
	"io/ioutil"
	"strings"
)

func main() {
	start := time.Now()
	file, err := os.Create("results.txt")
	ch := make(chan string)
	for _, url := range os.Args[1:] {
		if !strings.HasPrefix(url, "http://"){
			url = "http://" + url
		}
		go fetch(url, ch)
	}
	for range os.Args[1:] {

		if err != nil {
			fmt.Fprintf(os.Stderr, "%v", err)
		}

		file.WriteString(<-ch +"\n")
	}
	fmt.Printf("%.2fs passed\n", time.Since(start).Seconds())

}

func fetch(url string, ch chan<- string) {
	start := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err)
		return
	}
	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close()
	if err != nil {
		ch <- fmt.Sprintf("Error occured while reading %s: %v", url, err)
		return
	}

	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url)
}
