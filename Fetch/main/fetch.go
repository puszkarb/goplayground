package main

import (
	"os"
	"net/http"
	"fmt"
	"time"
	"io"
	"strings"
)

//Goals: 		Shows downloaded page given as URL address in txt file

func main() {
	now := time.Now()
	for _, url := range os.Args[1:] {
		if !strings.HasPrefix(url, "http://") {
			url = "http://" + url
		}
		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}
		b, err := io.Copy(os.Stdout, resp.Body)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: copying %s: %v\n", url, err)
			os.Exit(1)
		}

		fmt.Printf("%s\n", b)
		fmt.Printf("STATUS ----------------- %s\n", resp.Status)
	}

	fmt.Println(time.Since(now))
}
