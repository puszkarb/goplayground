package main

import (
	"math/rand"
	"github.com/MaxHalford/gago"
	"time"
	"fmt"
)

var Target = []byte("To be or not to be")

const MutationRate = 0.005

type Organism struct {
	DNA []byte
	Fitness float64
}

func (o Organism) Evaluate() (float64, error){
	score := 0

	for i:=0; i< len(o.DNA) ; i++ {
		if o.DNA[i] == Target[i] {
			score++
		}
	}
	f := float64(score) / float64(len(o.DNA))
	o.Fitness = f
	return -f , nil
}

func (o Organism) Mutate( rng *rand.Rand){
	for i:=0; i < len(o.DNA) ; i ++ {
		if rng.Float64() < MutationRate {
			o.DNA[i]= byte(rng.Intn(95) + 32)
		}
	}
}

func (o Organism) Crossover (genome gago.Genome, rng *rand.Rand){
		o2 := genome.(Organism)

		mid := rand.Intn(len(o2.DNA))

	for i:=0 ; i<len(o.DNA) ; i++ {
		if i>mid {
			o.DNA[i] = o2.DNA[i]
		}
	}

}

func (o Organism) Clone() gago.Genome{

	clon := make([]byte, len(o.DNA))
	for i := 0; i < len(o.DNA) ; i++{
		clon[i] = o.DNA[i]
	}
	organism := Organism{
		DNA: clon,
		Fitness: o.Fitness,
	}

	organism.Evaluate()
	return organism
}

func organismFactory(rng *rand.Rand) gago.Genome {

	newDna := make([]byte, len(Target))
	for i := 0; i < len(Target) ; i++{
		newDna[i] = byte(rand.Intn(95) + 32)
	}
	organism := Organism{
		DNA: newDna,
		Fitness: 0.0,
	}
	return organism
}


func main(){
	start := time.Now()
	ga:= gago.Generational(organismFactory)
	err := ga.Initialize()
	found := false
	generation := 0

	fmt.Printf("%v",ga)

	if err != nil{
		fmt.Println("Handle damn errors gopher!")
	}
	fmt.Printf("Best fitness at generation 0: %f\n", ga.HallOfFame[0].Fitness)
	for !found {
		generation++

		err = ga.Evolve()
		if err!= nil{
			fmt.Println("Handle damn error gopher!")
		}

		fmt.Printf("Best fitness at generation %d: %f    ----> %s\n",generation,ga.HallOfFame[0].Fitness, string(ga.HallOfFame[0].Genome.(Organism).DNA))

		if ga.HallOfFame[0].Fitness == float64(-1) {
			found = true
		}

	}
	fmt.Println("It all takes: ",  time.Since(start))



}