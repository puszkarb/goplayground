package main

import (
	"github.com/MaxHalford/gago"
	"fmt"
	m "math"
	"math/rand"

	"time"
)

type Vector []float64

func (X Vector) Evaluate() ( float64, error){

	//Dropdown wave function description

	var (
		numerator = 1 + m.Cos(12*m.Sqrt(m.Pow(X[0],2)+m.Pow(X[1], 2)))
		denominator = 0.5 * (m.Pow(X[0],2) + m.Pow(X[1],2)) + 2
		)

	return -numerator/denominator, nil
}

// Mutate a Vector by resampling each element from a normal distribution
// with probability equals 0.8

func (X Vector) Mutate(rng *rand.Rand){
	gago.MutNormalFloat64(X, 0.8, rng)
}

//Cross over a vector  with another Vector by applying uniform crossover

func (X Vector) Crossover ( Y gago.Genome, rng *rand.Rand){
	gago.CrossUniformFloat64(X, Y.(Vector), rng)
}

//Clone a vector to produce a new one that points to a different slice

func (X Vector) Clone() gago.Genome{
	var Y = make(Vector, len(X))
	copy(Y,X)
	return Y
}

//VectorFactory returns a random vector by generating 2 vaylues uniformally
//distributed between -10 ,10

func VectorFactory(rng *rand.Rand) gago.Genome{
	return Vector(gago.InitUnifFloat64(2,-1000,1000,rng))
}

func main(){
	var start = time.Now()
	var ga = gago.Generational(VectorFactory)
	var err = ga.Initialize()
	if err != nil{
		fmt.Println("Handle damn errors gopher!")

	}

	fmt.Printf("Best fitness at generation 0: %f\n", ga.HallOfFame[0].Fitness)
	for i := 1; i< 100; i++ {
		err = ga.Evolve()
		if err!= nil{
			fmt.Println("Handle damn error gopher!")
		}
		fmt.Printf("Best fitness at generation %d: %f\n",i,ga.HallOfFame[0].Fitness)
	}
	fmt.Println("It all takes: ",  time.Since(start))
}

