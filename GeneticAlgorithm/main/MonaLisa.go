package main

import (
	"image"
	"math/rand"
	"math"
	"sort"
	"time"
	"fmt"
	"os"
	"image/png"
	"bytes"

	"strconv"
)

//Mutation Rate is the rate of mutation

var MutationRate = 0.0004

// PopSize is the size of the population
var PopSize = 250

//PoolSize is the max size of the pool
var PoolSize = 30

// FitnessLimit is the fitness of the evolved image we are satisfied with
var FitnessLimit int64 = 7500

type Being struct {
	DNA *image.RGBA
	Fitness int64
}

func createBeing(target *image.RGBA) (being Being){
	being = Being{
		DNA: createRandomImageFrom(target),
		Fitness: 0,
	}
	being.calcFitness(target)
	return
}

func createRandomImageFrom(img *image.RGBA) (created *image.RGBA){
	pix := make([]uint8, len(img.Pix))
	rand.Read(pix)
	created = &image.RGBA{
		Pix: pix,
		Stride: img.Stride,
		Rect: img.Rect,
	}
	return
}

func (b *Being) calcFitness(target *image.RGBA){
	difference := diff(b.DNA, target)
	if difference == 0 {
		b.Fitness = 1
	}
	b.Fitness = difference

}

func diff(a,b *image.RGBA) (d int64){
	d = 0
	for i:=0 ; i < len(a.Pix) ; i++ {
		d+= int64(squareDifference(a.Pix[i], b.Pix[i]))
	}
	return int64(math.Sqrt(float64(d)))
}

func squareDifference(x,y uint8) uint64 {
	d := uint64(x) - uint64(y)
	return d*d
}

func createPool(population []Being, target *image.RGBA) (pool []Being){
	pool = make([]Being,0)

	sort.SliceStable(population, func(i, j int) bool {
		return population[i].Fitness < population[j].Fitness
	})

	top := population[0 : PoolSize +1]
	if top[len(top )-1].Fitness - top[0].Fitness == 0{
		pool = population
		return
	}

	for i := 0 ; i < len(top) -1  ; i++ {
		num := (top[PoolSize].Fitness - top[i].Fitness)
		for n:= int64(0) ; n < num ; n++{
			pool = append(pool, top[i])
		}

	}
	return
}

func crossover(d1 Being, d2 Being) Being{
	pix := make([]uint8, len(d1.DNA.Pix))

	child:= Being{
		DNA: &image.RGBA{
			Pix: pix,
			Stride: d1.DNA.Stride,
			Rect: d1.DNA.Rect,
		},
		Fitness: 0,
	}

	mid := rand.Intn(len(d1.DNA.Pix))
	for i:=0; i<len(d1.DNA.Pix) ; i++ {
		if i > mid {
			child.DNA.Pix[i] = d1.DNA.Pix[i]
		} else {
			child.DNA.Pix[i] = d2.DNA.Pix[i]
		}
	}
	return child
}

func (b *Being) mutate(){
	for i :=0 ; i < len(b.DNA.Pix); i++{
		if rand.Float64() < MutationRate {
			b.DNA.Pix[i] = uint8(rand.Intn(255))
		}
	}
}

func naturalSelection(pool []Being, population []Being, target *image.RGBA ) []Being {
	next := make([]Being,len(population))

	for i:= 0 ; i < len(population); i++ {
		r1, r2 := rand.Intn(len(pool)), rand.Intn(len(pool))

		a:= pool[r1]
		b:= pool[r2]

		child:= crossover(a,b)
		child.mutate()
		child.calcFitness(target)

		next[i] = child

	}

	return next
}
func createBeingPopulation (target *image.RGBA) (population []Being){
	population = make([]Being, PopSize)
	for i:= 0; i < PopSize; i++ {
		population[i] = createBeing(target)
	}
	return
}

func getBestBeing(population []Being) Being {
	best := int64(0)
	index := 0

	for i :=0; i< len(population); i++ {
		if population[i].Fitness > best {
			index = i
			best = population[i].Fitness
		}
	}
	return population[index]
}

func save(filePath string, rgba *image.RGBA){
	imgFile, err := os.Create(filePath)
	defer imgFile.Close()
	if err != nil {
		fmt.Println("Cannot create file:", err)
	}

	png.Encode(imgFile, rgba.SubImage(rgba.Rect))
}

func load(filePath string) *image.RGBA{
	imgFile, err := os.Open(filePath)
	defer imgFile.Close()
	if err != nil {
		fmt.Println("Cannot read file:", err)
	}

	img, _ , err := image.Decode(imgFile)
	if err != nil {
		fmt.Println("Cannot decode file:", err)
	}
	return img.(*image.RGBA)
}

var name = "print"
var counter = 0
func printImage(img *image.RGBA, number string){

	var buf bytes.Buffer
	buf.WriteString(name)
	buf.WriteString(number)
	buf.WriteString(".png")

	save(buf.String(),img)
	counter++
	/*png.Encode(&buf, img)
	imgBase64Str := base64.StdEncoding.EncodeToString(buf.Bytes())
	fmt.Printf("\x1b]1337;File=inline=1:%s\a\n", imgBase64Str)
	*/
	}

func main(){
	start := time.Now()
	rand.Seed(time.Now().UTC().UnixNano())
	target := load("./m.png")
	printImage(target, "0")
	population := createBeingPopulation(target)

	found := false
	generation := 0

	for !found{
		generation++
		bestBeing := getBestBeing(population)
		if bestBeing.Fitness < FitnessLimit{
			found = true
		} else {
			pool := createPool(population,target)
			population = naturalSelection(pool, population, target)
			if generation % 100 == 0 {
				sofar := time.Since(start)
				fmt.Printf("\nTime taken so far: %s | generation: %d | fitness: %d | pool size: %d", sofar, generation, bestBeing.Fitness, len(pool))
				save("./evolved.png", bestBeing.DNA)
				fmt.Println()
				printImage(bestBeing.DNA,strconv.Itoa(generation))
			}
		}
		}
	elapsed := time.Since(start)
	fmt.Printf("\nTotal time taken: %s\n", elapsed)

}



